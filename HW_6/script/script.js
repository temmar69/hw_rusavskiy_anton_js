function Human(name,age) {
	this.name = name;
	this.age = age;
	document.write(`Имя будет тебе  ${name} ! И лет тебе ${age} <br> <br>`)

}

const a = new Human(`Anton`,27);
const humansArr = [a  ,new Human(`Oleg`,18),new Human(`Alena`,17),new Human(`Oksana`,17)] //a { name:`Anton`,age:27 }

function humansSort(humans,dir) {
	const array = humans.sort(function(a,b) {
		return dir >= 1 ? a.age - b.age : b.age - a.age;   
	})
	return array; 
}
document.write(`Больше нуля возрастание, меньше - убывание <br> <br>`) 
const choto = humansSort(humansArr,0);
document.write(`VOT ONO ${JSON.stringify(choto)}<br> <br>`) 
const choto2 = humansSort(humansArr,1);
document.write(`VOT ONO ${JSON.stringify(choto2)}<br> <br>`) 
 

function Human2(name,age) {
	this.name = name;
	this.age = age;
	// method
	this.print = function() {
		document.write(`Имя будет тебе  ${name} ! И лет тебе ${age} <br> <br>`);
	} 
}
// static method of class
Human2.humansSort = function (humans,dir) {
	const array = humans.sort(function(a,b) {
		return dir >= 1 ? a.age - b.age : b.age - a.age;   
	})
	return array; 
}
// prototip
Human2.prototype.print2 = function() {
		document.write(`Имя будет тебе  ${this.name} ! И лет тебе ${this.age} <br> <br>`);
};
const qaq = new Human2('Oleg',99);
qaq.print2(); // protptip
document.write(JSON.stringify(Human2.humansSort([qaq],1)));


